# Geospatial Processing Singularity

A repo to build a general-purpose container for geospatial processing and analysis on DCC (using public data) that is bootstrapped from the [Rocker geospatial image](https://rocker-project.org/images/).

## Building

- A new container build will only be initiated by a modification to the Singularity.def file
- The built container file will only be pushed to the Duke OIT registry when a commit is tagged (ideally following the current version naming convention)
    + Note that to push, the container must be re-built first.

## Pulling

To pull the container file to DCC, use the following command:

```
export TAG_NAME="v001"; singularity pull --force --dir /opt/apps/community/od_chsi_rstudio oras://gitlab-registry.oit.duke.edu/ts415/geospatial-processing-singularity:${TAG_NAME}
```

## Running

The primary method of running the container is Open OnDemand via the Jupyter Singularity commmunity app.

## Notes
