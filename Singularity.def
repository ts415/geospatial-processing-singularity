BootStrap: docker
From: rocker/geospatial:latest

%labels
  Maintainer Tyler Schappe
  Image_Name geospatial_singularity

%help
  This container is bootstrapped from the Rocker Geospatial container and will run RStudio Server with tidyverse and standard geospatial processing tools. 

%apprun rserver
  exec rserver "${@}"

%runscript
  exec rserver "${@}"

%environment
  export SHELL=/bin/bash
  export LC_ALL=en_US.UTF-8
  export LANG=en_US.UTF-8
  export LANGUAGE=en_US.UTF-8
  # export MANUAL_BIN="/opt/bin" # doesn't do anything in singularity
  # export MANUAL_SHARE="/opt/share" # doesn't do anything in singularity
  export PATH=/opt/bin:/usr/lib/rstudio-server/bin:${PATH}:/usr/lib/rstudio-server/bin/quarto/bin/tools

%post

  ############
  # R packages
  ############

  # General packages
  Rscript -e 'install.packages(pkgs = c("foreach",
                                        "doParallel",
                                        "reshape2",
                                        "ggpattern",
                                        "ggrepel",
                                        "magick",
                                        "flextable",
                                        "emmeans",
                                        "viridisLite",
                                        "table1",
                                        "here"
                                        ),
                              quiet = TRUE
                                        
  )'

  # Spatial packages
  Rscript -e 'install.packages(pkgs = c("tigris",
                                        "tidycensus",
                                        "acs",
                                        "CARBayes",
                                        "spBayes",
                                        "rnaturalearth",
                                        "rnaturalearthdata",
                                        "googleway"
                                        ),
                                quiet = TRUE
  )'

